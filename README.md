# Lacuna-in-68k-Assembly
A game developed in Motorola 68000 Assembly Language as part of a project at FIEA (Fall 2015).

View a small demo video at https://www.youtube.com/watch?v=Zoqdq77wAXM

### How to Play - 

Download Easy68k, which is an emulator for the 68000 processor, frmo http://www.easy68k.com/

Clone this repository, load lacuna.x68 in Easy68k and run it.
